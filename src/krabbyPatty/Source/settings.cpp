#include "Headers/settings.h"

Settings::Settings()
{
    _sound = true;
    _mode = Mode::EasyMode;
}

void Settings::setSound(int isSoundOn)
{
    if (!isSoundOn)
        _sound = false;
    else
        _sound = true;
}

void Settings::setMode(Mode mode)
{
    _mode = mode;
}

auto Settings::getSound() -> bool
{
    return _sound;
}

auto Settings::getMode() -> Mode
{
    return _mode;
}

Settings::~Settings() = default;
