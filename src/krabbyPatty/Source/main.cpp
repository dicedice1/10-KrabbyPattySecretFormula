#include <QApplication>
#include <QFile>
#include <QFontDatabase>
#include <QSplashScreen>
#include <QTimer>

#include "Headers/widget.h"

auto main(int argc, char *argv[]) -> int
{
    QApplication a(argc, argv);
    Widget w;

    a.setWindowIcon(QIcon(":/images/icon1.png"));

    auto *splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/images/splash.png"));
    splash->show();

    QTimer::singleShot(5000, splash, SLOT(close()));
    QTimer::singleShot(5000, &w, SLOT(show()));

    return a.exec();
}
