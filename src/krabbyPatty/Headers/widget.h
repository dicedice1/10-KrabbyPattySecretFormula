#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include "leveldata.h"
#include "ranglist.h"
#include "score.h"
#include "settings.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

  public:
    Widget(QWidget *parent = nullptr);
    void onEsc(QKeyEvent *event);
    ~Widget();

  private slots:

    void updateScore();
    void createLevel();

    void startButton_clicked();
    void quitButton_clicked();
    void settingsButton_clicked();
    void pushButton_clicked();
    void checkSound_stateChanged(int arg1);
    void rbEasy_clicked();
    void rbHard_clicked();
    void rangListButton_clicked();
    void pushButtonMainMenu_clicked();
    void helpButton_clicked();
    void buttonBox_accepted();
    void buttonBox_rejected();
    void on_Give_up_clicked();

  private:
    Ui::Widget *ui;
    Settings *settings;
    RangList *ranglist;
    LevelData *levelData;
    void fontSetUp();
};
#endif // WIDGET_H
