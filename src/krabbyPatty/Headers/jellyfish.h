#ifndef JELLYFISH_H
#define JELLYFISH_H

#include <QRandomGenerator>

#include "deadlybarrier.h"

class Jellyfish : public DeadlyBarrier
{
  public:
    explicit Jellyfish(qreal playerWidth);

    void move() override;
    int getSteps();
    int getMoveY();
    int getWidth();
    int getHeight();
    const int max_steps = 60;

    ~Jellyfish();

  private:
    int steps = 0;
    int move_y = 1;
    qreal width = 0;
    qreal height = 0;
    QRandomGenerator *generator;
    void stepForward();
    void changeDirectionIfNeeded();
};

#endif // JELLYFISH_H
